import { LitElement, html, css } from "lit-element";

export class ComponentSearch extends LitElement{
    static get properties(){
        return{
            valorInput: {type:String}
        }
    }

    constructor(){
        super();
        this.valorInput = '';
    }

    _limpiar(){
        console.log(this.valorInput);
        this.valorInput = '';
        console.log(this.valorInput);
    }

    _getCharacters(e){
        //console.log(e.target.value)
        this.valorInput = e.target.value;
    }

    _searchName(){
        console.log(this.valorInput);
    }

    render(){
        return html`
        <div>
            <input type="text" @input=${this._getCharacters} value=${this.valorInput}>
            <button @click="${this._searchName}">Buscar Empleado</button>
            <button @click="${this._limpiar}">Limpiar</button>
        </div>
        `;
    }
}

customElements.define('component-search', ComponentSearch);