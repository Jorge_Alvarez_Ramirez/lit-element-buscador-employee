import { LitElement, html, css } from "lit-element";
import './component-search'

export class ComponentTabla extends LitElement{
    static get styles(){
        return css `
        .buscador{
            margin-top:50px;
            height:30px;
        }
        table{
            background: white;
            color: black;
            font-size: 18px;
            border: 1px solid gray;
            display:table;
            width: 80%;
        }
        thead{
            background-color: #34a4eb;
        }
        th, td{
            width: 10%;
            text-align: left;
            vertical-align: top;
        }
        `;
    }

    static get properties(){
        return{
            listaempleados: {type: Array},
            mostrarEmpleados: {type:Array},
            empleadoBuscado: {type: Array},
            encontroEmpleado: {type: Boolean},
            ordenarlista: {type: String}
        }
    }

    constructor(){
        super();
        this.listaempleados = [];
        this.mostrarEmpleados= this.listaempleados;
        this.empleadoBuscado=[];
        this.encontroEmpleado =false;
        this.ordenarlista = "Asc";
    }

    firstUpdated(){
        this.traerDatos();
    }
    
    traerDatos(){
        fetch("http://dummy.restapiexample.com/api/v1/employees",{method: "GET"})
        .then((response) => response.json())
        .then((data) => {
            this.listaempleados = data.data;
            this._mostrarTodos();
            this._ordenar(this.listaempleados);
        })
        .catch((error)=>{console.warn('Algo sucedio!!!', error);
        alert("Algo salio mal, intenta recargar la página.")
        })
    }

    dataHandler(e){
        this.listaempleados.forEach(element => {
            if(element.employee_name.toLowerCase().includes(e.detail.toLowerCase())){
                this.encontroEmpleado = true;
                this.empleadoBuscado.push(element)
            }
        });
        if(this.encontroEmpleado){
            this.mostrarEmpleados = this.empleadoBuscado;
            this.empleadoBuscado=[];
            this.encontroEmpleado =false;
        }else{
            alert('No se encontraron empleados')
        }
    }

    _metodoOrdenar(e){
        this.ordenarlista = e.target.value;
        this._ordenar(this.mostrarEmpleados);
    }

    _ordenar(array){
        if(this.ordenarlista === "Asc"){
            array.sort((o1, o2)=>{
                if(o1.employee_name < o2.employee_name){
                    return -1;
                }else if(o1.employee_name > o2.employee_name){
                    return 1;
                }else{
                    return 0;
                }
            })
        }else if(this.ordenarlista === "Des"){
            array.reverse();
        }
    }

    _mostrarTodos(){
        this._ordenar(this.listaempleados)
        this.mostrarEmpleados = this.listaempleados;
    }
    
    render(){
        return html`
            <div class="buscador">
                <component-search @my-event='${this.dataHandler}'></component-search>
            </div>
            <div id="tabla-empleados">
                <section>
                    <button @click="${this._mostrarTodos}">Mostrar todos</button>
                    <select @change="${this._metodoOrdenar}">
                        <option value="Asc">Ascendente</option>
                        <option value="Des">Descendente</option>
                    </select>
                </section>
                <table>
                    <caption>Empleados</caption>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Salario</th>
                            <th>Edad</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${this.mostrarEmpleados.map(empleado => {
                            return html`
                            <tr>
                                <th>${empleado.employee_name}</th>
                                <th>$ ${empleado.employee_salary}</th>
                                <th>${empleado.employee_age}</th>
                            </tr>`
                        })}
                    </tbody>
                </table>            
            </div>
        `;
    }
}

customElements.define('component-tabla', ComponentTabla);